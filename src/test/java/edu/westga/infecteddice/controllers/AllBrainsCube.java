package edu.westga.infecteddice.controllers;

import java.awt.Color;

import edu.westga.infecteddice.model.BrainFace;
import edu.westga.infecteddice.model.Cube;

/**
 * A determistic Cube in the sense that all faces are brains.  Used only for testing.
 * 
 * @author lewisb
 *
 */
class AllBrainsCube extends Cube {

    /**
     * Creates a new AllBrainsCube.
     */
    public AllBrainsCube() {
        for (int i = 0; i < 6; i++) {
            super.addFace(new BrainFace(this));
        }
    }

    @Override
    public Color getColor() {
        throw new UnsupportedOperationException();
    }
}
