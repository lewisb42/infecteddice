package edu.westga.infecteddice.controllers;

import edu.westga.infecteddice.model.BrainFace;
import edu.westga.infecteddice.model.GreenCube;
import edu.westga.infecteddice.model.RedCube;
import edu.westga.infecteddice.model.ShotgunFace;

class Utils {

    /**
     * Helper for when I need a new brain face.
     * 
     * @return a new brain face
     */
    static BrainFace newBrain() {
        return new BrainFace(new GreenCube());
    }
    
    /**
     * Helper for when I need a new shotgun face.
     */
    static ShotgunFace newShotgun() {
        return new ShotgunFace(new RedCube());
    }

}
