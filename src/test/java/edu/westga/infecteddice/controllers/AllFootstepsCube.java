package edu.westga.infecteddice.controllers;

import java.awt.Color;

import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.FootstepsFace;

/**
 * A deterministic Cube in the sense that all faces are footsteps. Used only for testing.
 * 
 * @author lewisb
 *
 */
public class AllFootstepsCube extends Cube {
    
    /**
     * Creates a new AllFootstepsCube.
     */
    public AllFootstepsCube() {
        for (int i = 0; i < 6; i++) {
            super.addFace(new FootstepsFace(this));
        }
    }
    
    @Override
    public Color getColor() {
        throw new UnsupportedOperationException();
    }
}
