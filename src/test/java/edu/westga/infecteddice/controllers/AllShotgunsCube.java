package edu.westga.infecteddice.controllers;

import java.awt.Color;

import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.ShotgunFace;

/**
 * A deterministic cube that always rolls a shotgun (removing any randomness). Use only for testing.
 * 
 * @author lewisb
 *
 */
class AllShotgunsCube extends Cube {
    
    /**
     * Creates a new AllShotgunsCube.
     */
    public AllShotgunsCube() {
        for (int i = 0; i < 6; i++) {
            super.addFace(new ShotgunFace(this));
        }
    }
    
    @Override
    public Color getColor() {
        throw new UnsupportedOperationException();
    }
}
