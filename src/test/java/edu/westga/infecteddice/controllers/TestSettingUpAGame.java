package edu.westga.infecteddice.controllers;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.DuplicateNameException;
import edu.westga.infecteddice.controllers.DefaultGameController;
import edu.westga.infecteddice.controllers.IGameController;

/**
 * Tests related to the configuration/setup phase of a GameController.
 * 
 * @author lewisb
 *
 */
public class TestSettingUpAGame {

    private IGameController game;
    
      
    /**
     * Setup code for all tests.
     */
    @Before
    public void setUp() {
        this.game = new DefaultGameController();
    }
    
    /**
     * Tests that a game cannot have zero players.
     */
    @Test(expected = IllegalStateException.class)
    public void canNotHaveZeroPlayers() {
        this.game.startGame();
    }

    /**
     * Tests that a game cannot have one player.
     * @throws DuplicateNameException n/a
     */
    @Test(expected = IllegalStateException.class)
    public void canNotHaveJustOnePlayer() throws DuplicateNameException {
        this.game.addPlayer("Joe Bob");
        this.game.startGame();
    }
    
    /**
     * Tests that a game cannot have nine players (the "max players plus 1" case).
     * 
     * @throws DuplicateNameException n/a
     */
    @Test(expected = IllegalStateException.class)
    public void canNotHaveNinePlayers() throws DuplicateNameException {
        for (int i = 0; i < 9; i++) {
            this.game.addPlayer("Player " + i);
        }
        this.game.startGame();
    }
    
    /**
     * Tests that a game cannot have way more than 8 players (the "way too many players" case).
     * 
     * @throws DuplicateNameException n/a
     */
    @Test(expected = IllegalStateException.class)
    public void canNotHave42Players() throws DuplicateNameException {
        for (int i = 0; i < 42; i++) {
            this.game.addPlayer("Player " + i);
        }
        this.game.startGame();
    }
    
    /**
     * Tests that a game cannot have two players with duplicate names.
     * 
     * @throws DuplicateNameException n/a
     */
    @Test(expected = DuplicateNameException.class)
    public void canNotHaveTwoPlayersWithDuplicateNames() throws DuplicateNameException {
        this.game.addPlayer("Joe Bob");
        this.game.addPlayer("Billy Boy");
        this.game.addPlayer("Sally Mae");
        this.game.addPlayer("Joe Bob");
    }
    
    
        
    /**
     * Tests that an already-running game can not be start()'ed
     * @throws DuplicateNameException n/a
     */
    @Test(expected = IllegalStateException.class)
    public void canNotStartAGameThatIsRunning() throws DuplicateNameException {
        this.game.addPlayer("Joe Bob");
        this.game.addPlayer("Billy Boy");
        this.game.addPlayer("Sally Mae");
        this.game.startGame();
        this.game.currentPlayerRolls();
        this.game.startGame();
    }
}
