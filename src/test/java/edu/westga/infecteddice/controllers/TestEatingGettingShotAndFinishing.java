package edu.westga.infecteddice.controllers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.ForcedTurnEndException;
import edu.westga.infecteddice.model.Player;

/**
 * This test class looks at the intersection of eating brains vs. getting shot.
 * @author lewisb
 *
 */
public class TestEatingGettingShotAndFinishing {

	private Player playa;
	private PlayerTurn turn1;
	private PlayerTurn turn2;
	private PlayerTurn turn3;
	
	/**
     * Called before each test.
     */
	@Before
	public void setup() {
		this.playa = new Player("test player");
		this.turn1 = new PlayerTurn(this.playa);
		this.turn2 = new PlayerTurn(this.playa);
		this.turn3 = new PlayerTurn(this.playa);
	}
	
	/**
	 * Simple case where some brains are eaten during a turn, but all are wiped out by 3 gunshots.
	 */
	@Test
	public void shouldFinishAfterEating3BrainsThenGettingShot3Times() {
		this.turn1.eatABrain(Utils.newBrain());
		this.turn1.eatABrain(Utils.newBrain());
		this.turn1.eatABrain(Utils.newBrain());
		try {
			this.turn1.shot(Utils.newShotgun());
			this.turn1.shot(Utils.newShotgun());
			this.turn1.shot(Utils.newShotgun());
			
			// should not make it to this point
			fail("Did not finish() properly");
		} catch (ForcedTurnEndException exc) {
			assertEquals(0, this.playa.getScore());
		}
	}
	
	/**
	 * Case where points were accumulated on prior turns -- those points should not
	 * be wiped out by 3 gunshots on a later turn.
	 */
	@Test
	public void turnEndedBy3ShotsShouldNotRemovePointsFromEarlierTurns() {
	    this.turn1.eatABrain(Utils.newBrain());
	    this.turn1.eatABrain(Utils.newBrain());
	    this.turn1.eatABrain(Utils.newBrain());
		this.turn1.finish();
		this.turn2.eatABrain(Utils.newBrain());
		this.turn2.eatABrain(Utils.newBrain());
		this.turn2.finish();
		this.turn3.eatABrain(Utils.newBrain());
		this.turn3.eatABrain(Utils.newBrain());
		try {
		    this.turn3.shot(Utils.newShotgun());
		    this.turn3.shot(Utils.newShotgun());
		    this.turn3.shot(Utils.newShotgun());
			fail("Did not finish() properly");
		} catch (ForcedTurnEndException exc) {
			assertEquals(5, this.playa.getScore());
		}
		
	}

}
