package edu.westga.infecteddice.controllers;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.ICup;
import edu.westga.infecteddice.model.Player;

@RunWith(EasyMockRunner.class)
public class TestRollingDiceOnATurn extends EasyMockSupport {
    
    @TestSubject
    private PlayerTurn turn = new PlayerTurn(new Player("test player"));
    
    @Mock
    private ICup cup;
    
    /**
     * Called before each test.
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        
    }

    /**
     * Tests taking the first three dice from the cup and rolling them.
     */
    @Test
    public void shouldRollFirstHand() {
        Cube[] gunStepsBrain = { new AllShotgunsCube(), new AllFootstepsCube(), new AllBrainsCube() };
        expect(this.cup.take(3))
            .andReturn(gunStepsBrain);
        replayAll();
        this.turn.rollHand();
        assertEquals(1, this.turn.shotgunsRolledThisTurn().size());
        assertEquals(1, this.turn.brainsRolledThisTurn().size());
        assertFalse(this.turn.isEnded());
    }
    
    /**
     * Tests to see that several faces of the same type can be rolled.
     */
    @Test
    public void shouldRollThreeBrains() {
        Cube[] cubes = { new AllBrainsCube(), new AllBrainsCube(), new AllBrainsCube() };
        expect(this.cup.take(3))
            .andReturn(cubes);
        replayAll();
        this.turn.rollHand();
        assertEquals(0, this.turn.shotgunsRolledThisTurn().size());
        assertEquals(3, this.turn.brainsRolledThisTurn().size());
        assertFalse(this.turn.isEnded());
    }
    
    /**
     * Tests the expectation that footsteps are re-rolled.
     */
    @Test
    public void shouldRerollThreeFootsteps() {
        Cube fs1 = new AllFootstepsCube();
        Cube fs2 = new AllFootstepsCube();
        Cube fs3 = new AllFootstepsCube();
        
        Cube[] firstRoll = { fs1, fs2, fs3 };
        expect(this.cup.take(3))
            .andReturn(firstRoll);
        // note that, on the 2nd roll, the footsteps from the previous should get re-rolled.
        Cube[] secondRoll = new Cube[0];
        expect(this.cup.take(0))
            .andReturn(secondRoll);
        replayAll();
        this.turn.rollHand();
        this.turn.rollHand();
        assertEquals(0, this.turn.shotgunsRolledThisTurn().size());
        assertEquals(0, this.turn.brainsRolledThisTurn().size());
        assertFalse(this.turn.isEnded());
    }
    
    /**
     * Tests whether successive rolls add dice up properly.  Also includes re-use of a footsteps roll.
     */
    @Test
    public void shouldProperlyAccumulateOnSuccessiveRolls() {
        AllFootstepsCube allFootsteps = new AllFootstepsCube();
        Cube[] firstRoll = { new AllShotgunsCube(), allFootsteps, new AllBrainsCube() };
        expect(this.cup.take(3))
            .andReturn(firstRoll);
        // note that, on the 2nd roll, the footsteps from the previous should get re-rolled.
        Cube[] secondRoll = { new AllBrainsCube(), new AllBrainsCube() };
        expect(this.cup.take(2))
            .andReturn(secondRoll);
        replayAll();
        this.turn.rollHand();
        this.turn.rollHand();
        assertEquals(1, this.turn.shotgunsRolledThisTurn().size());
        assertEquals(3, this.turn.brainsRolledThisTurn().size());
        assertFalse(this.turn.isEnded());
    }
    
    /**
     * Tests that three shotguns in a single roll ends the turn.
     */
    @Test
    public void threeShotgunsInSingleRollShouldEndTurn() {
        Cube[] gunGunGun = { new AllShotgunsCube(), new AllShotgunsCube(), new AllShotgunsCube() };
        expect(this.cup.take(3))
            .andReturn(gunGunGun);
        replayAll();
        this.turn.rollHand();
        assertEquals(3, this.turn.shotgunsRolledThisTurn().size());
        assertEquals(0, this.turn.brainsRolledThisTurn().size());
        assertTrue(this.turn.isEnded());
    }
    
    /**
     * Tests that three shotguns on successive rolls ends the turn
     */
    @Test
    public void threeShotgunsInSuccessiveRollsShouldEndTurn() {
        Cube[] firstRoll = { new AllShotgunsCube(), new AllBrainsCube(), new AllFootstepsCube() };
        expect(this.cup.take(3))
            .andReturn(firstRoll);
        Cube[] secondRoll = {new AllShotgunsCube(), new AllShotgunsCube() };
        expect(this.cup.take(2))
            .andReturn(secondRoll);
        replayAll();
        this.turn.rollHand();
        this.turn.rollHand();
        assertTrue(this.turn.isEnded());
    }
    
    /**
     * Tests that a turn can not continue after it is ended.
     */
    @Test(expected = IllegalStateException.class)
    public void shouldNotBeAbleToRollAfterTurnFinished() {
        Cube[] gunGunGun = { new AllShotgunsCube(), new AllShotgunsCube(), new AllShotgunsCube() };
        expect(this.cup.take(3))
            .andReturn(gunGunGun);
        replayAll();
        // should finish the hand
        this.turn.rollHand();
        // should throw state exception
        this.turn.rollHand();
    }
}
