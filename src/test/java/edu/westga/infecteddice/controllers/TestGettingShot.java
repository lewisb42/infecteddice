package edu.westga.infecteddice.controllers;


import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.ForcedTurnEndException;
import edu.westga.infecteddice.model.Player;

/**
 * Tests of the PlayerTurn.shot() method
 * 
 * @author lewisb
 *
 */
public class TestGettingShot {

	private Player playa;
	private PlayerTurn turn;
	
	/**
     * Called before each test.
     */
	@Before
	public void setup() {
		this.playa = new Player("test player");
		this.turn = new PlayerTurn(this.playa);
	}
	

	
	/**
	 * Case where shots on subsequent rolls total to exactly 3.  Should throw ForcedTurnEndException.
	 * 
	 * @throws ForcedTurnEndException this is the expected outcome
	 */
	@Test(expected = ForcedTurnEndException.class)
	public void shouldFinishIfGetsOneMoreShotAfterPreviouslyGettingTwoShots() throws ForcedTurnEndException {
		this.turn.shot(Utils.newShotgun());
		this.turn.shot(Utils.newShotgun());
		this.turn.shot(Utils.newShotgun());
	}
	
	/**
	 * Ensures that once a PlayerTurn is finish()'d that shot() can no longer be called on it.  Should
	 * throw IllegalStateException.
	 * 
	 * @throws ForcedTurnEndException this should never be thrown
	 */
	@Test(expected = IllegalStateException.class)
	public void shouldNotGetShotAfterTurnIsFinished() throws ForcedTurnEndException {
		this.turn.finish();
		this.turn.shot(Utils.newShotgun());
	}
}
