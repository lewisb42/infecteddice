package edu.westga.infecteddice.controllers;

import static org.junit.Assert.*;

import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.ICup;
import edu.westga.infecteddice.model.Player;

/**
 * Tests creation of a PlayerTurn (and those effects on Player)
 * 
 * @author lewisb
 *
 */
public class TestCreatingPlayerTurn {

	private Player playa;
	private PlayerTurn turn;
	
	/**
	 * Called before each test.
	 */
	@Before
	public void setup() {
		this.playa = new Player("test player");
		this.turn = new PlayerTurn(this.playa);
	}
	
	/**
	 * Test to ensure a PlayerTurn can not be created with a null Player.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void canNotMakePlayerTurnWithNullPlayer() {
		new PlayerTurn(null);
	}

	/**
	 * Test to check that a successful PlayerTurn creation with a new Player leaves that Player with
	 * zero points.
	 */
	@Test
	public void eatingNoBrainsGivesPlayerScoreOfZero() {
		this.turn.finish();
		assertEquals(0,  this.playa.getScore());
	}
}
