package edu.westga.infecteddice.controllers;

import static org.junit.Assert.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.DefaultGameController;
import edu.westga.infecteddice.controllers.IGameController;


public class TestPlayingAGame {
    private IGameController game;
    
    // the actual strings here aren't important, only that I have variables
    // with constant values.
    private static final String PLAYER_0 = "player 0";
    private static final String PLAYER_1 = "player 1";
    private static final String PLAYER_2 = "player 2";
    private static final String PLAYER_3 = "player 3";
    
    /**
     * We'll try rolling this many times to force a "3 shotguns" end-of-turn. May
     * need to increase if the relevant test(s) fail too often.
     */
    private static final int MAX_ROLLS = 100;
    
    // used to track the current player as turns pass from one to another
    private StringProperty currentPlayerName;
    
    
    /**
     * Common setup code for the tests.
     * 
     * @throws Exception if something goes really wrong?
     */
    @Before
    public void setUp() throws Exception {
        this.game = new DefaultGameController();
        this.game.addPlayer(PLAYER_0);
        this.game.addPlayer(PLAYER_1);
        this.game.addPlayer(PLAYER_2);
        this.game.addPlayer(PLAYER_3);
        this.currentPlayerName = new SimpleStringProperty();
        this.currentPlayerName.bind(this.game.currentPlayerName());
    }

    /**
     * Tests that first player added is the current player when game starts.
     */
    @Test
    public void whenGameStartsPlayer0ShouldBeCurrent() {
        this.game.startGame();
        assertEquals(PLAYER_0, this.currentPlayerName.get());
    }
    
    /**
     * Tests that players are properly cycled through an entire round of passing.
     */
    @Test
    public void shouldCycleEntireRoundOfPlayersInOrder() {
        this.game.startGame();
        assertEquals(PLAYER_0, this.currentPlayerName.get());
        this.game.currentPlayerPasses();
        assertEquals(PLAYER_1, this.currentPlayerName.get());
        this.game.currentPlayerPasses();
        assertEquals(PLAYER_2, this.currentPlayerName.get());
        this.game.currentPlayerPasses();
        assertEquals(PLAYER_3, this.currentPlayerName.get());
        this.game.currentPlayerPasses();
        assertEquals(PLAYER_0, this.currentPlayerName.get());
    }
  
    /**
     * Tests that we get a forced end-of-turn.  This one is a little dicey -- the idea is just keep re-rolling the
     * same player until the odds catch up with us and they get three or more shotguns.  We're trying to beat the randomness
     * here, so I guess there's a slim chance the test could fail simply because that player got REALLY lucky.
     */
    @Test
    public void shouldGetForcedEndOfTurn() {
        this.game.startGame();
        String firstPlayer = this.currentPlayerName.get();
        for (int i = 0; i < MAX_ROLLS; i++) {
            this.game.currentPlayerRolls();
            if (!firstPlayer.equals(this.currentPlayerName.get())) {
                // success! the player name has changed, indicating a forced end-of-turn,
                // so we're done.
                return;
            }
        }
        
        // if we made it this far it means we never advanced to a new PlayerTurn, so fail
        fail("Could not force end-of-turn.  Try re-running the test in case you just got bad odds.");
    }
}
