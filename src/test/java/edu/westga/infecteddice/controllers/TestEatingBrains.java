package edu.westga.infecteddice.controllers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.Player;

/**
 * Tests of the PlayerTurn.eatBrains() method.
 * 
 * @author lewisb
 *
 */
public class TestEatingBrains {

	private Player playa;
	private PlayerTurn turn;
	
	/**
     * Called before each test.
     */
	@Before
	public void setup() {
		this.playa = new Player("test player");
		this.turn = new PlayerTurn(this.playa);
	}

		
	/**
	 * Explicitly eating one brain gives a player score of 1.
	 * 
	 * This is the 1 case for int.
	 */
	@Test
	public void eatingOneBrainCorrectlyScoresPlayer() {
	    this.turn.eatABrain(Utils.newBrain());
		this.turn.finish();
		assertEquals(1,  this.playa.getScore());
	}
	
	/**
	 * Explicitly eating 3 brains gives a player score of 3.
	 * 
	 * This is a (valid-value) border case.
	 */
	@Test
	public void eatingThreeBrainsCorrectlyScoresPlayer() {
	    this.turn.eatABrain(Utils.newBrain());
	    this.turn.eatABrain(Utils.newBrain());
	    this.turn.eatABrain(Utils.newBrain());
		this.turn.finish();
		assertEquals(3,  this.playa.getScore());
	}
	
	/**
	 * Case to check that IllegalStateException is thrown when a PlayerTurn is finish()'d but an
	 * attempt is made to eatSomeBrains() on it.
	 */
	@Test(expected = IllegalStateException.class)
	public void shouldNotEatBrainsOnceFinished() {
		this.turn.finish();
		this.turn.eatABrain(Utils.newBrain());
	}

}
