package edu.westga.infecteddice.model;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.ForcedTurnEndException;

/**
 * Test various (mostly illegal) configurations of the Cube class.
 * @author lewisb
 *
 */
public class TestBuildingCubes {

	/**
	 * We need a stub Face class.  We could use one of the actual classes from the model, but this keeps the tests
	 * independent of those classes.
	 * @author lewisb
	 *
	 */
	private class TestFace extends Face {

		protected TestFace(Cube cube) {
			super(cube);
		}

		@Override
		public void effect(PlayerTurn turn) throws ForcedTurnEndException {
			
		}
		
	}
	
	/**
	 * A Cube that has no faces.  We should not be allowed to call public methods on it.
	 * @author lewisb
	 *
	 */
	private class FacelessCube extends Cube {
		
		/**
		 * Need a way to access the protected getFace() method for test coverage.  This is it.
		 * @param index pass-through to Cube.getFace(index)
		 */
		public void callGetFace(int index) {
			this.getFace(index);
		}

        @Override
        public Color getColor() {
            throw new UnsupportedOperationException();
        }
	};
	
	private FacelessCube noFaceCube;
	
	/**
	 * A Cube that has 3 faces.  We should not be allowed to call public methods on it.
	 * This satisfies the "somewhere in the middle" case.
	 * @author lewisb
	 *
	 */
	private class ThreeFaceCube extends FacelessCube {
		public ThreeFaceCube() {
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
		}
	}
	
	private ThreeFaceCube threeFaceCube;
	
	/**
	 * A Cube that has 7 faces.  We should not be allowed to call public methods on it.
	 * This satisfied the "+1" border case.
	 * @author lewisb
	 *
	 */
	private class SevenFaceCube extends FacelessCube {
		public SevenFaceCube() {
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
			super.addFace(new TestFace(this));
		}
	}
	
	/**
	 * A Cube that has 7 faces.  We should not be allowed to call public methods on it.
	 * This satisfies the "way paast the border" case
	 * @author lewisb
	 *
	 */
	private class TenFaceCube extends FacelessCube {
		public TenFaceCube() {
			for (int i = 0; i < 10; i++) {
				super.addFace(new TestFace(this));
			}
		}
	}
	
	/**
     * Called before each test.
     */
	@Before
	public void setup() {
		this.noFaceCube = new FacelessCube();
		this.threeFaceCube = new ThreeFaceCube();
	}
	
	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToRollFacelessCube() {
		this.noFaceCube.roll();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToGetFaceFromFacelessCube() {
		this.noFaceCube.callGetFace(3);
	}
	
	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToRollThreeFaceCube() {
		this.threeFaceCube.roll();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToGetExistingFaceFromThreeFaceCube() {
		this.noFaceCube.callGetFace(1);
	}
	
	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToGetNonExistantFaceFromThreeFaceCube() {
		this.noFaceCube.callGetFace(3);
	}
	
	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToMakeSevenFaceCube() {
		new SevenFaceCube();
	}
	
	@Test(expected = IllegalStateException.class)
	public void shouldNotBeAbleToMakeTenFaceCube() {
		new TenFaceCube();
	}
}
