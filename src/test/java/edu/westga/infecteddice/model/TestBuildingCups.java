package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.ICup;
import edu.westga.infecteddice.model.AbstractCup;
import edu.westga.infecteddice.model.GreenCube;
import edu.westga.infecteddice.model.RandomizedCup;
import edu.westga.infecteddice.model.RedCube;
import edu.westga.infecteddice.model.YellowCube;

public class TestBuildingCups {

	private ICup cup;
	
	/**
	 * Called before  each test
	 */
	@Before
	public void setup() {
		this.cup = new RandomizedCup();
	}
	
	@Test
	public void newCupShouldBeProperlyPopulated() {
		int redCount = 0;
		int greenCount = 0;
		int yellowCount = 0;
		
		for (int i = 0; i < 13; i++) {
			Cube[] cube = this.cup.take(1);
			if (cube[0] instanceof RedCube) {
				redCount++;
			} else if (cube[0] instanceof GreenCube) {
				greenCount++;
			} else if (cube[0] instanceof YellowCube) {
				yellowCount++;
			} else {
				fail("found an unknown cube!");
			}
		}
		
		assertEquals(6, greenCount);
		assertEquals(4, yellowCount);
		assertEquals(3, redCount);
	}

}
