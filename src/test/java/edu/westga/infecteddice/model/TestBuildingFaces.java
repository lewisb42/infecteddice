package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.ForcedTurnEndException;

public class TestBuildingFaces {

	/**
	 * Minimal Face that exposes internal protected members.
	 * @author lewisb
	 *
	 */
	private class TestFace extends Face {

		/**
		 * Creates a TestFace with the given Cube
		 * @param cube the face's enclosing cube
		 */
		public TestFace(Cube cube) {
			super(cube);
		}

		/**
		 * Exposes the protected Face.getCube() method
		 * @return super.getCube()
		 */
		public Cube callGetCube() {
			return super.getCube();
		}
		
		@Override
		public void effect(PlayerTurn turn) throws ForcedTurnEndException {
			
		}
		
	}
	
	/**
	 * Minimal Cube class.
	 * @author lewisb
	 *
	 */
	private class TestCube extends Cube {
	    @Override
        public Color getColor() {
            throw new UnsupportedOperationException();
        }
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCreateFaceWithNullCube() {
		new TestFace(null);
	}
	
	@Test
	public void shouldSuccessfullyCreateFace() {
		TestCube cube = new TestCube();
		TestFace face = new TestFace(cube);
		assertEquals(cube, face.callGetCube());
	}
}
