package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.infecteddice.model.Player;

/**
 * Tests to check proper parameters and creation state for the Player(String) constructor.
 * 
 * @author lewisb
 *
 */
public class TestCreatingPlayer {

    private Player player;
    
    /**
     * Player(String) constructor null check.
     */
    @Test(expected = IllegalArgumentException.class)
    public void playerNameCanNotBeNull() {
        new Player(null);
    }
    
    /**
     * Player(String) constructor empty string check.
     */
    @Test(expected = IllegalArgumentException.class)
    public void playerNameCanNotBeEmpty() {
        new Player("");
    }

    /**
     * Player(String) constructor good value check.
     */
    @Test
    public void playerHasAName() {
        this.player = new Player("Joe Bob");
        assertEquals("Joe Bob", this.player.getName());
    }
}
