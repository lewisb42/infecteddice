package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.ICup;
import edu.westga.infecteddice.model.AbstractCup;
import edu.westga.infecteddice.model.GreenCube;
import edu.westga.infecteddice.model.RedCube;
import edu.westga.infecteddice.model.YellowCube;


public class TestTakingCubesFromCups {


	private ICup cup;

	
	/**
     * Called before  each test
     */
	@Before
	public void setup() {
		this.cup = new AbstractCup() {
			
		};
	}
	
	@Test
	public void emptyCupShouldNotGiveOutMoreDice() {
		for (int i = 0; i < 13; i++) {
			this.cup.take(1);
		}
		
		Cube[] emptyHand = this.cup.take(1);
		assertEquals(0, emptyHand.length);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotTakeNegativeOneDiceFromCup() {
		this.cup.take(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotTakeReallyNegativeNumberOfDiceFromCup() {
		this.cup.take(-100);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotTakeFourDiceFromCup() {
		this.cup.take(4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotTakeLargeNumberOfDiceFromCup() {
		this.cup.take(100);
	}
	
	@Test
	public void shouldBeAbleToTakeOneCubeFromCup() {
		Cube[] cubes = this.cup.take(1);
		assertEquals(1, cubes.length);
	}
	
	@Test
	public void shouldBeAbleToTakeTwoDiceFromCup() {
		Cube[] cubes = this.cup.take(2);
		assertEquals(2, cubes.length);
	}
	
	@Test
	public void shouldBeAbleToTakeThreeDiceFromCup() {
		Cube[] cubes = this.cup.take(3);
		assertEquals(3, cubes.length);
	}
}
