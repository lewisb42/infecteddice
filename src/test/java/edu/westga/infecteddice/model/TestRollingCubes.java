package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.infecteddice.model.BrainFace;
import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.FootstepsFace;
import edu.westga.infecteddice.model.GreenCube;
import edu.westga.infecteddice.model.RedCube;
import edu.westga.infecteddice.model.ShotgunFace;
import edu.westga.infecteddice.model.YellowCube;

public class TestRollingCubes {

	private static final int NUM_ROLLS = 10000;
	private static final double DELTA = 0.01;
	
	@Test
	public void greenCubeShouldHaveCorrectProbabilities() {
		GreenCube cube = new GreenCube();
		this.cubeProbabilitiesHelper(cube, 3, 1, 2);
	}
	
	@Test
	public void redCubeShouldHaveCorrectProbabilities() {
		RedCube cube = new RedCube();
		this.cubeProbabilitiesHelper(cube, 1, 3, 2);
	}
	
	@Test
	public void yellowCubeShouldHaveCorrectProbabilities() {
		YellowCube cube = new YellowCube();
		this.cubeProbabilitiesHelper(cube, 2, 2, 2);
	}
	
	/**
	 * Rolls the given Cube a large number of times, summing up the various faces that result, then
	 * calculating each Face's probability and comparing it to the indicated parameters.
	 * @param cube the Cube to roll
	 * @param numBrains the number of brains faces this Cube should have.
	 * @param numBlasts the number of shotgun faces this Cube should have.
	 * @param numFootsteps the number of footsteps faces this Cube should have.
	 */
	private void cubeProbabilitiesHelper(Cube cube, int numBrains, int numBlasts, int numFootsteps) {
		double blastCount = 0;
		double brainCount = 0;
		double footstepsCount = 0;
		for (int i = 0; i < NUM_ROLLS; i++) {
			Face face = cube.roll();
			if (face instanceof BrainFace) {
				brainCount++;
			} else if (face instanceof ShotgunFace) {
				blastCount++;
			} else if (face instanceof FootstepsFace) {
				footstepsCount++;
			} else {
				fail("invalid face found!");
			}
		}
		
		assertEquals("brains probability incorrect; try re-running test", numBrains / 6.0, brainCount / NUM_ROLLS, DELTA);
		assertEquals("footsteps probability incorrect; try re-running test", numFootsteps / 6.0, footstepsCount / NUM_ROLLS, DELTA);
		assertEquals("shotgun probability incorrect; try re-running test", numBlasts / 6.0, blastCount / NUM_ROLLS, DELTA);
	}

}
