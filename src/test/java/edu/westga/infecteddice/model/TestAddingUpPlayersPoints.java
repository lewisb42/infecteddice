package edu.westga.infecteddice.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.infecteddice.controllers.PlayerTurn;
import edu.westga.infecteddice.model.Player;

public class TestAddingUpPlayersPoints {

	private Player playa;
	
	/**
     * Called before each test.
     */
	@Before
	public void setup() {
		this.playa = new Player("test player");
	}
	
	@Test
	public void newPlayerShouldHaveZeroPoints() {
		assertEquals(0, this.playa.getScore());
	}
	
	@Test
	public void shouldAddZeroPoints() {
		this.playa.addToScore(5);
		this.playa.addToScore(0);
		assertEquals(5, this.playa.getScore());
	}
	
	@Test
	public void shouldHave5Points() {
		this.playa.addToScore(5);
		assertEquals(5, this.playa.getScore());
	}
	
	@Test
	public void shouldAdd5ThenAdd10Points() {
		this.playa.addToScore(5);
		this.playa.addToScore(10);
		assertEquals(15, this.playa.getScore());
	}
	
	@Test
	public void shouldAddMaxTurnPoints() {
		this.playa.addToScore(PlayerTurn.MAX_TURN_POINTS);
		assertEquals(PlayerTurn.MAX_TURN_POINTS, this.playa.getScore());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAddOneMoreThanMaxPoints() {
		this.playa.addToScore(PlayerTurn.MAX_TURN_POINTS + 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAddALotMoreThanMaxPoints() {
		this.playa.addToScore(PlayerTurn.MAX_TURN_POINTS + 100);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAddNegativeOnePoints() {
		this.playa.addToScore(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAddLargeNegativeNumberOfPoints() {
		this.playa.addToScore(-100);
	}
}
