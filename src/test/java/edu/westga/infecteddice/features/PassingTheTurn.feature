Feature: After any roll a player may pass the dice to the next player, permanently keeping any scored brains
  accumulated on this turn.

  Scenario Outline: Player passes
    Given Player has amassed <count> brains so far and fewer than three shotguns
    And Player's current total is <prevTotal>
    When Player passes
    Then Player's permanent score is increased to <total>
    And the next Player begins a turn

    Examples: 
      | count | prevTotal | total |
      |     0 |         0 |     0 |
      |     3 |        10 |    13 |
      |     2 |         5 |     7 |
      |     0 |         5 |     5 |
