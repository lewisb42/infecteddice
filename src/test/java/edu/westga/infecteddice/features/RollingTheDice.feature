Feature: Rolling the dice
  Any rolled brains are set aside into a brains pile
  Any rolled shotguns are set aside into a shotguns pile; if this pile grows to 3 or
    more dice the turn ends
  Any rolled footsteps may be re-rolled or the player may end the turn


  Scenario Outline: Player rolls three dice and the turn ends due to too many shotguns
    Given The player has previously acquired <gunCount> shotguns on this turn
    When The player rolls <rolledGuns> shotguns, bringing the total number of shotguns to three or greater
    Then The player's turn end with zero points awarded for the turn.

    Examples:
    | gunCount | rolledGuns |
    |        2 |          1 |
    |        1 |          3 |
    |        2 |          2 |
    |        0 |          3 |

  Scenario Outline: Player rolls some shotguns and the turn does not end
    Given The player has previously acquired <brainCount> brains and <gunCount> shotguns on this turn
    When The player rolls <rolledGuns> shotguns and <rolledBrains> brains, bringing the total number of shotguns to no more than two
    Then The player now has <newBrainCount> brains and <newGunCount> shotguns
    And The player may continue their turn

    Examples:
    | brainCount | gunCount | rolledGuns | rolledBrains | newBrainCount | newGunCount |
    |          0 |        0 |          0 |            0 |             0 |           0 |
    |          1 |        0 |          1 |            0 |             1 |           1 |
    |          3 |        2 |          0 |            2 |             5 |           2 |
    |          3 |        1 |          1 |            2 |             5 |           2 |

