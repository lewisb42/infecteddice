package edu.westga.infecteddice.features.step_definitions;

import static org.junit.Assert.*;

import org.junit.Test;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PassingTheTurnSteps {

	@Given("^Player has amassed (\\d+) brains so far and fewer than three shotguns$")
	public void player_has_amassed_brains_so_far_and_fewer_than_three_shotguns(int numBrains) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^Player's current total is (\\d+)$")
	public void player_s_current_total_is(int currentTotal) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^Player passes$")
	public void player_passes() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^Player's permanent score is increased to (\\d+)$")
	public void player_s_permanent_score_is_increased_to(int totalScore) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^the next Player begins a turn$")
	public void the_next_Player_begins_a_turn() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}
