package edu.westga.infecteddice.features.step_definitions;

import static org.junit.Assert.*;

import org.junit.Test;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RollingTheDiceSteps {

	@Given("^The player has previously acquired (\\d+) shotguns on this turn$")
	public void the_player_has_previously_acquired_shotguns_on_this_turn(int numShotguns) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^The player rolls (\\d+) shotguns, bringing the total number of shotguns to three or greater$")
	public void the_player_rolls_shotguns_bringing_the_total_number_of_shotguns_to_three_or_greater(int numShotgunsRolled) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^The player's turn end with zero points awarded for the turn\\.$")
	public void the_player_s_turn_end_with_zero_points_awarded_for_the_turn() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^The player has previously acquired (\\d+) brains and (\\d+) shotguns on this turn$")
	public void the_player_has_previously_acquired_brains_and_shotguns_on_this_turn(int numBrains, int numShotguns) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^The player rolls (\\d+) shotguns and (\\d+) brains, bringing the total number of shotguns to no more than two$")
	public void the_player_rolls_shotguns_and_brains_bringing_the_total_number_of_shotguns_to_no_more_than_two(int numShotgunsRolled, int numBrainsRolled) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^The player now has (\\d+) brains and (\\d+) shotguns$")
	public void the_player_now_has_brains_and_shotguns(int totalBrains, int totalShotguns) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^The player may continue their turn$")
	public void the_player_may_continue_their_turn() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


}
