package edu.westga.infecteddice.views;

import javafx.collections.ObservableList;
import edu.westga.infecteddice.model.Face;

/**
 * ListCell for the current hand display.
 * 
 * @author lewisb
 *
 */
public class CurrentHandListCell extends TallyMarkListCell {

    private static final String STYLE_CLASS = "current-hand";
    /**
     * Creates a new CurrentHandListCell.
     */
    public CurrentHandListCell() {
        super();
    }
    
    @Override
    public void updateItem(Face item, boolean empty) {
        super.updateItem(item, empty);
        
        // add an appropriate style class to properly size this cell.
        ObservableList<String> styleClasses = this.getStyleClass();
        styleClasses.add(CurrentHandListCell.STYLE_CLASS);
    }
    
}
