package edu.westga.infecteddice.views;

import java.awt.Color;
import java.net.URL;
import java.util.ResourceBundle;

import edu.westga.infecteddice.controllers.DuplicateNameException;
import edu.westga.infecteddice.controllers.DefaultGameController;
import edu.westga.infecteddice.controllers.IGameController;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.Player;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;

/**
 * Controller class for the HomeScreen GUI.
 * @author lewisb
 *
 */
public class HomeScreenController {

    private IGameController game;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableColumn<Player, String> playerNameColumn;
    
    @FXML
    private TableColumn<Player, Number> playerScoreColumn;
    
    @FXML
    private TableView<Player> scoreboardTableView;

    @FXML
    private Label currentPlayerNameLabel;

    @FXML
    private Label numBrainsLabel;

    @FXML
    private Label numShotgunsLabel;

    @FXML
    private Button rollButton;

    @FXML
    private Button passButton;
    
    @FXML
    private ListView<Face> brainsTallyListView;
    
    @FXML
    private ListView<Face> shotgunsTallyListView;
    
    @FXML
    private ListView<Face> currentRollListView;

    @FXML
    void initialize() {
        assert scoreboardTableView != null : "fx:id=\"scoreboardTableView\" was not injected: check your FXML file 'HomeScreen.fxml'.";
        assert currentPlayerNameLabel != null : "fx:id=\"currentPlayerNameLabel\" was not injected: check your FXML file 'HomeScreen.fxml'.";
        assert numBrainsLabel != null : "fx:id=\"numBrainsLabel\" was not injected: check your FXML file 'HomeScreen.fxml'.";
        assert numShotgunsLabel != null : "fx:id=\"numShotgunsLabel\" was not injected: check your FXML file 'HomeScreen.fxml'.";
        assert rollButton != null : "fx:id=\"rollButton\" was not injected: check your FXML file 'HomeScreen.fxml'.";
        assert passButton != null : "fx:id=\"passButton\" was not injected: check your FXML file 'HomeScreen.fxml'.";

        this.game = new DefaultGameController();
        this.scoreboardTableView.setItems(this.game.getScoreboardModel());
        this.playerNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        this.playerScoreColumn.setCellValueFactory(cellData -> cellData.getValue().scoreProperty());
        
        // TODO: hard-coded 4-player game for now; replace with interactive adding of players
        try {
            this.game.addPlayer("Joe");
            this.game.addPlayer("Sally");
            this.game.addPlayer("Donald");
            this.game.addPlayer("Jarvis");
            this.game.startGame();
        } catch (DuplicateNameException exp) {
            // TODO Auto-generated catch block
            exp.printStackTrace();
        }
        
        this.currentPlayerNameLabel.textProperty().bind(this.game.currentPlayerName());
        
        this.brainsTallyListView.setItems(this.game.brainsRolledThisTurn());
        this.brainsTallyListView.setCellFactory(cellData -> new TallyMarkListCell());
        
        this.shotgunsTallyListView.setItems(this.game.shotgunsRolledThisTurn());
        this.shotgunsTallyListView.setCellFactory(cellData -> new TallyMarkListCell());
        
        this.currentRollListView.setItems(this.game.currentDiceInHand());
        this.currentRollListView.setCellFactory(cellData -> new CurrentHandListCell());
    }

    
    
    /**
     * Actions occurring when the Roll button is clicked.
     */
    public void onRollButtonClick() {
        this.game.currentPlayerRolls();
    }
    
    /**
     * Actions occurring when the Pass button is clicked.
     */
    public void onPassButtonClick() {
        this.game.currentPlayerPasses();
    }
}

