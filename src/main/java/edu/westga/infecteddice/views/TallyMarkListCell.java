package edu.westga.infecteddice.views;

import javafx.collections.ObservableList;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;
import edu.westga.infecteddice.model.Face;

/**
 * Probably a throw-away class, but wanted to reuse some code for the tally-marks for brains and shotguns.
 * @author lewisb
 *
 */
class TallyMarkListCell extends TextFieldListCell<Face> {
    
    /**
     * Creates the list cell using the given char as the tally mark
     */
    public TallyMarkListCell() {
        this.setEditable(false);
        this.setConverter(new StringConverter<Face>() {
            @Override
            public String toString(Face face) {
                return FaceDisplayDecoder.getSymbol(face);
            }

            @Override
            public Face fromString(String string) {
                throw new UnsupportedOperationException();
            }
        });
        
        
    }
    
    @Override
    public void updateItem(Face item, boolean empty) {
        super.updateItem(item, empty);
        
        // add an appropriate style class to properly color this cell
        ObservableList<String> styleClasses = this.getStyleClass();
        styleClasses.removeAll(FaceDisplayDecoder.GREEN_CUBE_STYLE_CLASS, FaceDisplayDecoder.YELLOW_CUBE_STYLE_CLASS, FaceDisplayDecoder.RED_CUBE_STYLE_CLASS);
        
        if (empty || item == null) {
            this.setText(null);
            this.setGraphic(null);
        } else {
            styleClasses.add(FaceDisplayDecoder.getStyleClass(item));
        }
    }
    
}
