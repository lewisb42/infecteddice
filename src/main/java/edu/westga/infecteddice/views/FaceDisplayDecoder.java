package edu.westga.infecteddice.views;

import java.awt.Color;

import edu.westga.infecteddice.model.BrainFace;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.FootstepsFace;
import edu.westga.infecteddice.model.GreenCube;
import edu.westga.infecteddice.model.RedCube;
import edu.westga.infecteddice.model.ShotgunFace;

/**
 * Utility methods for matching a Face object with its proper visual display.
 * 
 * @author lewisb
 *
 */
public class FaceDisplayDecoder {

    private static final String BLAST_CHAR = "\uD83D\uDCA5";
    private static final String BRAIN_CHAR = "\uD83D\uDC68";
    private static final String FOOTSTEPS_CHAR = "\uD83D\uDC63";
    
    /**
     * A CSS style class for red cubes.
     */
    public static final String RED_CUBE_STYLE_CLASS = "red-cube";
    
    /**
     * A CSS style class for green cubes.
     */
    public static final String GREEN_CUBE_STYLE_CLASS = "green-cube";
    
    /**
     * A CSS style class for yellow cubes.
     */
    public static final String YELLOW_CUBE_STYLE_CLASS = "yellow-cube";
    
    /**
     * Gets a Unicode string representing a shotgun blast
     *  
     * @param face a shotgun face
     * @return a shotgun blast Unicode string
     */
    public static String getSymbol(ShotgunFace face) {
        return FaceDisplayDecoder.BLAST_CHAR;
    }
    
    /**
     * Gets a Unicode string representing a brain
     *  
     * @param face a brain face
     * @return a brain Unicode string
     */
    public static String getSymbol(BrainFace face) {
        return FaceDisplayDecoder.BRAIN_CHAR;
    }
    
    /**
     * Gets a Unicode string representing footsteps
     *  
     * @param face a footsteps face
     * @return a footsteps Unicode string
     */
    public static String getSymbol(FootstepsFace face) {
        return FaceDisplayDecoder.FOOTSTEPS_CHAR;
    }
    
    /**
     * Gets a Unicode string for the given face.
     *  
     * @param face a face
     * @return its appropriate unicode string
     * @throws IllegalArgumentException if there is no valid Face type matching the face
     */
    public static String getSymbol(Face face) {
        if (face instanceof BrainFace) {
            return FaceDisplayDecoder.getSymbol((BrainFace)face);
        } else if (face instanceof ShotgunFace) {
            return getSymbol((ShotgunFace)face);
        } else if (face instanceof FootstepsFace) {
            return getSymbol((FootstepsFace)face);
        } else {
            throw new IllegalArgumentException("not a valid face type");
        }
    }
    
    /**
     * Gets the style class for this face, based on its color.
     * 
     * @param face the face
     * @return its CSS style class
     */
    public static String getStyleClass(Face face) {
        Color color = face.getColor();
        if (color.equals(Color.GREEN)) {
            return GREEN_CUBE_STYLE_CLASS;
        } else if (color.equals(Color.YELLOW)) {
            return YELLOW_CUBE_STYLE_CLASS;
        } else if (color.equals(Color.RED)) {
            return RED_CUBE_STYLE_CLASS;
        }
        
        throw new IllegalArgumentException("invalid color");
    }
}
