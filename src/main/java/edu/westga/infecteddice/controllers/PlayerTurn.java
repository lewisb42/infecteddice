package edu.westga.infecteddice.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import edu.westga.infecteddice.model.BrainFace;
import edu.westga.infecteddice.model.Cube;
import edu.westga.infecteddice.model.FootstepsFace;
import edu.westga.infecteddice.model.ICup;
import edu.westga.infecteddice.model.Face;
import edu.westga.infecteddice.model.ForcedTurnEndException;
import edu.westga.infecteddice.model.FaceListProperty;
import edu.westga.infecteddice.model.Player;
import edu.westga.infecteddice.model.RandomizedCup;
import edu.westga.infecteddice.model.ShotgunFace;

/**
 * Represents the game state of one turn for one player.
 * 
 * @author lewisb
 *
 */
public class PlayerTurn {
	/**
	 * The maximum number of points that could be awarded in a turn.
	 */
	public static final int MAX_TURN_POINTS = 13;
	
	/**
	 * The number of dice that get rolled each time.
	 */
	private static final int NUM_DICE_TO_ROLL = 3;
	
	private Player player;
	private ICup cup;
	private boolean ended;
	
	// this is the *next* set of dice to be rolled
	private ArrayList<Cube> nextHand;
	
	// total brains rolled during this turn
	private FaceListProperty brainsRolledThisTurn;
	
	// total shotguns rolled during this turn
	private FaceListProperty shotgunsRolledThisTurn;
	
	// just the faces rolled *this hand* (could include footsteps)
	private FaceListProperty facesRolledThisHand;
	
	/**
	 * Creates a new player turn using a cup with randomized dice.
	 * 
	 * @param player the Player who is taking this turn
	 */
	public PlayerTurn(Player player) {
		this.init(player, new RandomizedCup());
	}
	
	/**
	 * Called by constructors to initialize stuff for the PlayerTurn.
	 * 
	 * @param player the Player who is taking this turn.
	 * @param cup the dice cup to use for this turn.
	 */
	protected void init(Player player, ICup cup) {
		if (player == null) {
			throw new IllegalArgumentException("player may not be null");
		}
		
		if (cup == null) {
			throw new IllegalArgumentException("cup may not be null");
		}
		
		this.player = player;
		this.cup = cup;
		this.ended = false;
		this.nextHand = new ArrayList<Cube>();
		
		this.brainsRolledThisTurn = new FaceListProperty();
		this.shotgunsRolledThisTurn = new FaceListProperty();
		this.facesRolledThisHand = new FaceListProperty();
	}
	
	/**
	 * helper method to throw an IllegalStateException if the turn has ended
	 */
	private void throwIllegaStateExceptionIfEnded() {
		if (this.ended) {
			throw new IllegalStateException("this turn has ended");
		}
	}
	
	/**
	 * The player has, in his turn, eaten a brain. Yum!
	 * 
	 * @param brainFace the brain that was rolled
	 */
	public void eatABrain(BrainFace brainFace) {
	    this.throwIllegaStateExceptionIfEnded();
		this.brainsRolledThisTurn.add(brainFace);
		this.facesRolledThisHand.add(brainFace);
	}
	
	/**
	 * The player has been shot.  Can force an end-of-turn if the total number of shots equals 3 or more.
	 * 
	 * @param shotgunFace the shotgun rolled
	 * @throws ForcedTurnEndException if the number of shotguns forces an end-of-turn.
	 */
	public void shot(ShotgunFace shotgunFace) throws ForcedTurnEndException {
		this.throwIllegaStateExceptionIfEnded();
		this.shotgunsRolledThisTurn.add(shotgunFace);
		
		if (this.shotgunsRolledThisTurn.size() >= 3) {
			throw new ForcedTurnEndException();
		}
		
		this.facesRolledThisHand.add(shotgunFace);
	}
	
	/**
	 * A runner escaped from the player.
	 * 
	 * @param footstepsFace the footsteps face that was rolled.
	 */
	public void escaped(FootstepsFace footstepsFace) {
		this.throwIllegaStateExceptionIfEnded();
		this.facesRolledThisHand.add(footstepsFace);
	}
	
	/**
	 * Removes a cube from the hand.
	 * 
	 * @param cube the cube to remove.
	 */
	public void removeFromHand(Cube cube) {
		this.nextHand.remove(cube);
	}
	
	/**
	 * Rolls a new hand of dice (including any footsteps from earlier rolls)
	 */
	public void rollHand() {
	    this.throwIllegaStateExceptionIfEnded();
	    this.facesRolledThisHand.clear();
		int numToReplenish = NUM_DICE_TO_ROLL - this.nextHand.size();
		this.nextHand.addAll(Arrays.asList(this.cup.take(numToReplenish)));
		

		// making a copy so Cubes can remove themselves from nextHand
		ArrayList<Cube> currentHand = new ArrayList<Cube>(this.nextHand);
		
		for (Cube cube: currentHand) {
			Face face = cube.roll();
			try {
				face.effect(this);
			} catch (ForcedTurnEndException exc) {
				// turn is over
			    this.brainsRolledThisTurn.clear();
				this.finish();
				return;
			}
		}
	}
	
	
	/**
	 * Completes this turn.
	 */
	public void finish() {
		this.throwIllegaStateExceptionIfEnded();
		if (this.shotgunsRolledThisTurn.size() < 3) {
			this.player.addToScore(this.brainsRolledThisTurn.size());
		}
		this.ended = true;
	}
	
	/**
	 * Queries whether the turn has ended or not.
	 * 
	 * @return true if ended; false if still ongoing
	 */
	public boolean isEnded() {
		return this.ended;
	}
	
	/**
	 * Property accessor for the list of brains rolled this turn.
	 * 
	 * @return the list property of brains rolled this turn.
	 */
	public FaceListProperty brainsRolledThisTurn() {
	    return this.brainsRolledThisTurn;
	}
	
	/**
     * Property accessor for the list of shotguns rolled this turn.
     * 
     * @return the list property of shotguns rolled this turn.
     */
    public FaceListProperty shotgunsRolledThisTurn() {
        return this.shotgunsRolledThisTurn;
    }
    
    /**
     * Property accessor for the list of dice faces rolled this hand.
     * These are the faces _just rolled_, not the running total.
     * 
     * @return the list property of dice faces rolled this hand.
     */
    public FaceListProperty facesRolledThisHand() {
        return this.facesRolledThisHand;
    }
}
