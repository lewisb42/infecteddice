package edu.westga.infecteddice.controllers;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import edu.westga.infecteddice.model.FaceListProperty;
import edu.westga.infecteddice.model.Player;

/**
 * Manages Players and PlayerTurns
 * @author lewisb
 *
 */
public class DefaultGameController implements IGameController {
    
    // properties exposed to JavaFX stuff
    private StringProperty currentPlayerNameProperty;
    private IntegerProperty numBrainsProperty;
    private IntegerProperty numShotgunsProperty;
    private FaceListProperty brainsRolledThisTurn;
    private FaceListProperty shotgunsRolledThisTurn;
    private FaceListProperty currentDiceInHand;
    
    /**
     * The maximum allowed players for a game.
     */
	private static final int MAX_PLAYERS = 8;
	
	/**
	 * The minimum allowed players for a game.
	 */
	private static final int MIN_PLAYERS = 2;
	
	private ObservablePlayerList players;
	
	private int currentPlayer;
	
	
	private PlayerTurn currentTurn; 
	
	/**
	 * The phases of gameplay.
	 * 
	 * @author lewisb
	 */
	private enum Phases {
	    
	    /**
	     * Configuration phase for GameController.  This the phase where players can be added.
	     */
	    GAME_SETUP,
	    
	    /**
	     * Game is in play.
	     */
	    GAME_RUNNING,
	    
	    /**
	     * Game has finished.
	     */
	    GAME_ENDED 
	};
	
	private Phases phase;
	
	/**
	 * Creates a GameController with the given number of players.
	 */
	public DefaultGameController() {
		this.players = new ObservablePlayerList();
		
		// setting this to -1 lets us re-use the changePlayerHelper() in startGame()
		this.currentPlayer = -1;
		this.phase = Phases.GAME_SETUP;
		
		this.currentPlayerNameProperty = new SimpleStringProperty();
		this.numBrainsProperty = new SimpleIntegerProperty();
		this.numShotgunsProperty = new SimpleIntegerProperty();
		
		this.brainsRolledThisTurn = new FaceListProperty();
		this.shotgunsRolledThisTurn = new FaceListProperty();
		this.currentDiceInHand = new FaceListProperty();
	}
	
	/**
	 * Checks that the GameController is configured correctly (e.g., has proper number of players, etc).
	 * 
	 * @throws IllegalStateException if improperly configured
	 */
	private void checkConfiguration() {
	    int numPlayers = this.players.size();
	    if (numPlayers < MIN_PLAYERS || numPlayers > MAX_PLAYERS) {
	        throw new IllegalStateException("invalid number of players");
	    }
	}
	
	/**
	 * Checks that the GameController is in the proper phase.  Used to keep from calling methods at the wrong time.
	 * 
	 * @param phase the phase we expect to be in.
	 * @throws IllegalStateException if not in the given phase.
	 */
	private void checkInPhase(Phases phase) {
	    if (!this.phase.equals(phase)) {
	        throw new IllegalStateException("game is in incorrect phase for the selected operation");
	    }
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#startGame()
     */
	@Override
    public void startGame() {
	    this.checkConfiguration();
	    this.checkInPhase(Phases.GAME_SETUP);
	    this.phase = Phases.GAME_RUNNING;
	    this.changePlayerHelper();
		this.updateGameState();
	}
	
	/**
	 * Gets the name of the current player (as "Player 1", "Player 2", etc.)
	 * 
	 * @return the name of the current player
	 */
	private String getCurrentPlayerName() {
	    this.checkConfiguration();
		return this.players.get(this.currentPlayer).getName();
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#currentPlayerName()
     */
	@Override
    public StringProperty currentPlayerName() {
	    return this.currentPlayerNameProperty;
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#brainsRolledThisTurn()
     */
	@Override
    public FaceListProperty brainsRolledThisTurn() {
	    return this.brainsRolledThisTurn;
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#shotgunsRolledThisTurn()
     */
	@Override
    public FaceListProperty shotgunsRolledThisTurn() {
	    return this.shotgunsRolledThisTurn;
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#currentDiceInHand()
     */
	@Override
    public FaceListProperty currentDiceInHand() {
	    return this.currentDiceInHand;
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#numShotgunsInPlay()
     */
	@Override
    public IntegerProperty numShotgunsInPlay() {
	    return this.numShotgunsProperty;
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#currentPlayerRolls()
     */
	@Override
    public void currentPlayerRolls() {
	    this.checkInPhase(Phases.GAME_RUNNING);
		this.currentTurn.rollHand();
		this.updateGameState();
		if (this.currentTurn.isEnded()) {
			this.changePlayerHelper();
		}
	}
	
	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#currentPlayerPasses()
     */
	@Override
    public void currentPlayerPasses() {
	    this.checkInPhase(Phases.GAME_RUNNING);
		this.currentTurn.finish();
		this.changePlayerHelper();
		
	}
	
	/**
	 * Ends the current turn and sets up the next turn (with the next player).
	 */
	private void changePlayerHelper() {
		//this.updateGameState();
		this.brainsRolledThisTurn.unbind();
		this.shotgunsRolledThisTurn.unbind();
		this.currentDiceInHand.unbind();
		this.currentPlayer = (this.currentPlayer + 1) % this.players.size();
		this.currentTurn = new PlayerTurn(this.players.get(this.currentPlayer));
		this.brainsRolledThisTurn.bind(this.currentTurn.brainsRolledThisTurn());
		this.shotgunsRolledThisTurn.bind(this.currentTurn.shotgunsRolledThisTurn());
		this.currentDiceInHand.bind(this.currentTurn.facesRolledThisHand());
		this.updateGameState();
	}
	

	/**
	 * Helper to notify any GameStateListeners of changes
	 */
	private void updateGameState() {
	    this.currentPlayerNameProperty.set(this.getCurrentPlayerName());
	    this.numBrainsProperty.set(this.currentTurn.brainsRolledThisTurn().size());
	    this.numShotgunsProperty.set(this.currentTurn.shotgunsRolledThisTurn().size());
	}

	/* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#addPlayer(java.lang.String)
     */
    @Override
    public void addPlayer(String name) throws DuplicateNameException {
        for (Player player: this.players) {
            if (player.getName().equals(name)) {
                throw new DuplicateNameException();
            }
        }
        this.players.add(new Player(name));     
    }

    /* (non-Javadoc)
     * @see edu.westga.infecteddice.controllers.IGameController#getScoreboardModel()
     */
    @Override
    public ObservableList<Player> getScoreboardModel() {
        return this.players;
    }
}
