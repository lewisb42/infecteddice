package edu.westga.infecteddice.controllers;

import java.util.ArrayList;

import edu.westga.infecteddice.model.Player;
import javafx.collections.ModifiableObservableListBase;

/**
 * Internal implementation of ObservableList<Player>.
 * 
 * @author lewisb
 *
 */
class ObservablePlayerList extends ModifiableObservableListBase<Player> {

    private ArrayList<Player> players = new ArrayList<Player>();
    
    @Override
    public Player get(int index) {
        return this.players.get(index);
    }

    @Override
    public int size() {
        return this.players.size();
    }

    @Override
    protected void doAdd(int index, Player element) {
        this.players.add(index, element);
    }

    @Override
    protected Player doSet(int index, Player element) {
        return this.players.set(index, element);
    }

    @Override
    protected Player doRemove(int index) {
        return this.players.remove(index);
    }

}
