package edu.westga.infecteddice.controllers;

/**
 * Exception thrown when a Player is created with a duplicate name as an existing player.
 * 
 * @author lewisb
 *
 */
public class DuplicateNameException extends Exception {

    private static final long serialVersionUID = -935067243082670711L;
    private static final String MESSAGE = "A Player with that name already exists";
    
    /**
     * Creates a DuplicateNameException with a default message.
     */
    public DuplicateNameException() {
        super(DuplicateNameException.MESSAGE);
    }

}
