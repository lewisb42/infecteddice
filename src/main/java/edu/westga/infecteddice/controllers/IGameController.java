package edu.westga.infecteddice.controllers;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import edu.westga.infecteddice.model.FaceListProperty;
import edu.westga.infecteddice.model.Player;

public interface IGameController {

    /**
     * Starts a new game.
     */
    void startGame();

    /**
     * Property for the current player's name.
     * 
     * @return bindable property for the current player's name.
     */
    StringProperty currentPlayerName();

    /**
     * Property for the list of brains in rolled this turn.
     * 
     * @return bindable property for the current list of brains showing on the board.
     */
    FaceListProperty brainsRolledThisTurn();

    /**
     * Property for the list of shotguns rolled this turn
     * 
     * @return bindable property for the current list of shotguns showing on the board.
     */
    FaceListProperty shotgunsRolledThisTurn();

    /**
     * Property for the list of dice currently in hand (i.e., just-roled)
     * 
     * @return bindable property for the dice just rolled
     */
    FaceListProperty currentDiceInHand();

    /**
     * Property for the number of shotguns in play.
     * 
     * @return bindable property for the current number of shotguns showing on the board.
     */
    IntegerProperty numShotgunsInPlay();

    /**
     * Indicates the current player rolls 3 dice, which may
     * include any footsteps previously rolled.  May cause the turn to end and pass
     * to the next player.
     * 
     * This can only happen while the game is running
     */
    void currentPlayerRolls();

    /**
     * Indicates the current player passes, adding any brains showing to their total score.
     * 
     * This can only happen while the game is running.
     */
    void currentPlayerPasses();

    /**
     * Creates a player with the given name.
     * 
     * @param name the player's name
     * @throws DuplicateNameException if there is already a player by that name
     */
    void addPlayer(String name) throws DuplicateNameException;

    /**
     * Returns an handle to an ObservableList of Player's that can be observed
     * by, e.g., a ListView widget.
     * 
     * @return an observable list of the players in the game
     */
    ObservableList<Player> getScoreboardModel();

}