package edu.westga.infecteddice.model;

/**
 * Thrown when the number of shotgun blasts forces an end-of-turn.
 * 
 * @author lewisb
 *
 */
public class ForcedTurnEndException extends Exception {

    private static final long serialVersionUID = 1712668039088612741L;

}
