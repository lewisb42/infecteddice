package edu.westga.infecteddice.model;

/**
 * An ICup is something that can hold and give out dice.
 * 
 * @author lewisb
 *
 */
public interface ICup {

	/**
	 * Fills the Cup with the appropriate numbers of red, green, and yellow dice.
	 */
	void refill();
	
	/**
	 * Removes some number of cubes from the cup.
	 * 
	 * @param numCubes the number of cubes to remove.
	 * @return an array with a randomly-selected set of cubes.  If the requested number of cubes is greater than the remaining
	 *    cubes, return whatever is left in the cup.  Returns an empty array if there were no cubes remaining in the cup.
	 * @throws IllegalArgumentException if numCubes is anything other than 1, 2, or 3
	 */
	Cube[] take(int numCubes);
	
}
