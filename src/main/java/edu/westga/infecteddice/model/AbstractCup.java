package edu.westga.infecteddice.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 * An abstract class to pull in the code common to all Cup classes.  The default constructor results in a 
 * predictable sequence of dice: 6 green, 3 red, and 4 yellow, in that order.
 * 
 * It is not expected to use this in production code -- RandomizedCup exists for that purpose.  AbstractCup contains the bulk of
 * RandomizedCup's code, however, and thus can be used for test coverage purposes.
 * 
 * @author lewisb
 *
 */
public abstract class AbstractCup implements ICup {

	private static final int NUM_GREEN_DICE = 6;
	private static final int NUM_RED_DICE = 3;
	private static final int NUM_YELLOW_DICE = 4;
	
	private ArrayList<Cube> dice;
	
	/**
	 * Creates a Cup populated with a full set of dice.  This is only to be used by child classes.
	 */
	protected AbstractCup() {
		this.dice = new ArrayList<Cube>();
		this.refill();
	}
	
	/**
	 * Allows child classes to access the list of dice.
	 * 
	 * @return the internal list of dice.
	 */
	protected ArrayList<Cube> getDice() {
		return this.dice;
	}
	
	@Override
	public void refill() {
	    this.dice.clear();
		for (int i = 0; i < AbstractCup.NUM_GREEN_DICE; i++) {
			this.dice.add(new GreenCube());
		}
		
		for (int i = 0; i < AbstractCup.NUM_RED_DICE; i++) {
			this.dice.add(new RedCube());
		}
		
		for (int i = 0; i < AbstractCup.NUM_YELLOW_DICE; i++) {
			this.dice.add(new YellowCube());
		}
	}
	
	@Override
	public Cube[] take(int numCubes) {
		if (numCubes < 0 || numCubes > 3) {
			throw new IllegalArgumentException("invalid number of cubes");
		}
	
		Cube[] hand = new Cube[numCubes];
		if (numCubes == 0) {
			return hand;
		}
		
		// if not enough dice left, return what's left,
		// refill the cup (minus the remaining dice), and take
		// enough extra dice from the cup to make a roll
		if (numCubes > this.dice.size()) {
		    int numToTake = numCubes = this.dice.size();
		    ArrayList<Cube> remaining = new ArrayList<Cube>(this.dice);
		    this.refill();
		    for (Cube cube: remaining) {
		        this.dice.remove(cube);
		    }
		    
		    for (int i = 0; i < numToTake; i++) {
		        remaining.add(this.dice.remove(0));
		    }
			return remaining.toArray(new Cube[0]);
		}
		
		for (int i = 0; i < numCubes; i++) {
			hand[i] = this.dice.remove(0);
		}
		
		return hand;
	}
}
