package edu.westga.infecteddice.model;

import edu.westga.infecteddice.controllers.PlayerTurn;

/**
 * A Face representing a shotgun blast that hit the zombie.
 * 
 * @author lewisb
 *
 */
public class ShotgunFace extends Face {

	/**
	 * Creates a ShotgunFace on the given cube.
	 * 
	 * @param cube the enclosing cube.
	 */
	public ShotgunFace(Cube cube) {
		super(cube);
	}
	
	@Override
	public void effect(PlayerTurn turn) throws ForcedTurnEndException {
		turn.shot(this);
		turn.removeFromHand(super.getCube());
	}

}
