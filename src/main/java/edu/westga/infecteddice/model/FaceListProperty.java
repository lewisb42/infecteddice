package edu.westga.infecteddice.model;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

/**
 * ListProperty for integers.
 * 
 * @author lewisb
 *
 */
public class FaceListProperty extends SimpleListProperty<Face> {

    /**
     * Creates a FaceListProperty backed by an empty list.
     */
    public FaceListProperty() {
        super(FXCollections.observableArrayList());
    }
}
