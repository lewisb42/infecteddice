package edu.westga.infecteddice.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import edu.westga.infecteddice.controllers.PlayerTurn;

/**
 * Represents one player in the game.
 * 
 * @author lewisb
 *
 */
public class Player {

	private IntegerProperty totalScore;
	private StringProperty name;
	
	/**
	 * Creates a new player.
	 * 
	 * @param name the player's name. may not be null or empty.
	 */
	public Player(String name) {
	    if (name == null) {
	        throw new IllegalArgumentException("name can not be null");
	    }
	    
	    if (name.isEmpty()) {
	        throw new IllegalArgumentException("name can not be empty");
	    }
		this.totalScore = new SimpleIntegerProperty(0);
		this.name = new SimpleStringProperty(name);
	}
	
	/**
	 * Gets the player's name.
	 * 
	 * @return the player's name.
	 */
	public String getName() {
	    return this.name.get();
	}
	
	/**
	 * Adds to the player's running score.
	 * 
	 * @param points the points to add
	 */
	public void addToScore(int points) {
		if (points < 0 || points > PlayerTurn.MAX_TURN_POINTS) {
			throw new IllegalArgumentException("invalid number of points");
		}
		int score = this.getScore() + points;
		this.totalScore.set(score);
	}
	
	/**
	 * Gets the player's score.  This only permanent points, i.e., points added to their running
	 * score at the end of a turn.  It does not include "temporary" points accrued while actively
	 * in a turn.
	 * 
	 * @return the player's current score.
	 */
	public int getScore() {
		return this.totalScore.get();
	}
	
	/**
	 * The name property of this player.
	 * 
	 * @return the player's name property
	 */
	public StringProperty nameProperty() {
	    return this.name;
	}
	
	/**
	 * The score property of this player.
	 * 
	 * @return the player's score property.
	 */
	public IntegerProperty scoreProperty() {
	    return this.totalScore;
	}
}
