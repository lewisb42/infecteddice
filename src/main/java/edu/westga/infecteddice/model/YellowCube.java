package edu.westga.infecteddice.model;

import java.awt.Color;
import java.util.Random;

/**
 * A YellowCube has 2 of each kind of face (shotgun, brain, and footsteps)
 * @author lewisb
 *
 */
public class YellowCube extends Cube {

    /**
     * Give us a random hashId.
     */
    private static final int hashId = (new Random()).nextInt();
    
	/**
	 * Creates a YellowCube with the appropriate faces.
	 */
	public YellowCube() {
		for (int i = 0; i < 2; i++) {
			super.addFace(new ShotgunFace(this));
			super.addFace(new BrainFace(this));
			super.addFace(new FootstepsFace(this));
		}
	}
	
	// overridden to properly implement the hashCode-equals contrace
    @Override
    public int hashCode() {
        return YellowCube.hashId;
    }

    @Override
    public Color getColor() {
        return Color.YELLOW;
    }
}
