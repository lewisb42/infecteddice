package edu.westga.infecteddice.model;

import java.awt.Color;
import java.util.Random;

/**
 * A Cube containing 1 brain, 3 shotguns, and 2 runners
 * @author lewisb
 *
 */
public class RedCube extends Cube {

    /**
     * Give us a random hashId.
     */
	private static final int hashId = (new Random()).nextInt();

    /**
	 * Creates a RedCube with the appropriate number of faces
	 */
	public RedCube() {
		super.addFace(new BrainFace(this));
		for (int i = 0; i < 3; i++) {
			super.addFace(new ShotgunFace(this));
		}
		super .addFace(new FootstepsFace(this));
		super .addFace(new FootstepsFace(this));
	}
	
	// overridden to properly implement the hashCode-equals contrace
    @Override
    public int hashCode() {
        return RedCube.hashId;
    }

    @Override
    public Color getColor() {
        return Color.RED;
    }
}
