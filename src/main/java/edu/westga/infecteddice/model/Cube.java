package edu.westga.infecteddice.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 * A die with six faces, each of which can be a shotgun, brain, or footsteps
 * 
 * @author lewisb
 *
 */
public abstract class Cube {

    private static Random rng = new Random();
	private ArrayList<Face> faces;
	
	
	/**
	 * Required to be called by child classes.
	 */
	protected Cube() {
		this.faces = new ArrayList<Face>();
	}
	
	/**
	 * Allows child classes to add a face to the Cube.
	 * 
	 * @param face the face to add
	 */
	protected void addFace(Face face) {
		this.faces.add(face);
		if (this.faces.size() > 6) {
			throw new IllegalStateException("too many faces");
		}
	}
	
	/**
	 * Allows child classes to get a specific face.
	 * 
	 * @param index the index of the face to get
	 * @return the currently-displayed face
	 */
	protected Face getFace(int index) {
		this.checkThatCubeHasSixFaces();
		return this.faces.get(index);
	}
	
	/**
	 * "Rolls" the die to get a Face
	 * @return a randomly-selected face from the roll.
	 */
	public Face roll() {
		this.checkThatCubeHasSixFaces();
		int index = Cube.rng.nextInt(6);
		return this.getFace(index);
	}
	
	/**
	 * Helper to check that this Cube has been properly constructed before calling its public methods.
	 * 
	 * @throws IllegalStateException if the Cube doesn't have exactly 6 faces.
	 */
	private void checkThatCubeHasSixFaces() {
		if (this.faces.size() != 6) {
			throw new IllegalStateException("attempted operation on Cube with invalid number of faces");
		}
	}
	
	/**
	 * A Cube is equal to another Cube if and only if they are of the same runtime class type.
	 * I.e., a GreenCube is equal to a GreenCube, but a RedCube is not equal to a YellowCube.
	 * 
	 * @param other the other cube
	 * @return true if they are the same color of cube
	 */
	public boolean equals(Cube other) {
	    return this.getClass().equals(other.getClass());
	}
	
	/**
	 * The color of this cube.
	 * 
	 * @return this cube's color.
	 */
	public abstract Color getColor();
}


