package edu.westga.infecteddice.model;

import edu.westga.infecteddice.controllers.PlayerTurn;

/**
 * A Face representing a brain a zombie has eaten.
 * 
 * @author lewisb
 *
 */
public class BrainFace extends Face {

	/**
	 * Creates a new BrainFace with the given containing cube.
	 * 
	 * @param cube the Cube that contains this BrainFace
	 */
	public BrainFace(Cube cube) {
		super(cube);
	}
	
	@Override
	public void effect(PlayerTurn turn) {
		turn.eatABrain(this);;
		turn.removeFromHand(super.getCube());
	}

}
