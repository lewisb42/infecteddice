package edu.westga.infecteddice.model;

import java.util.Collections;

/**
 * A cup where the dice are randomly-selected.
 * 
 * @author lewisb
 *
 */
public class RandomizedCup extends AbstractCup {

	/**
	 * Creates a new cup where the dice are randomly-selected.
	 */
	public RandomizedCup() {
		super();
	}
	
	@Override
	public void refill() {
	    super.refill();
	    Collections.shuffle(super.getDice());
	}
}
