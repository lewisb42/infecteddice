package edu.westga.infecteddice.model;

import java.awt.Color;

import edu.westga.infecteddice.controllers.PlayerTurn;

/**
 * Represent a face on a die (i.e., Cube).  Child classes should be immutable.
 * 
 * @author lewisb
 *
 */
public abstract class Face {
	private Cube cube;
	
	/**
	 * Associates this Face with the Cube that has it.
	 * @param cube the Cube that encloses this Face
	 */
	protected Face(Cube cube) {
		if (cube == null) {
			throw new IllegalArgumentException("cube may not be null");
		}
		this.cube = cube;
	}
	
	/**
     * The default constructor may not be used by child classes (forcing them to use the protected constructor)
     */
    @SuppressWarnings("unused")
    private Face() {
        
    }
	
	/**
	 * Allows child classes to find out which cube contains them.
	 * 
	 * @return the enclosing cube
	 */
	protected Cube getCube() {
		return this.cube;
	}
	
	/**
	 * The color of the cube associated with this face.
	 * 
	 * @return the parent cube's color.
	 */
	public Color getColor() {
	    return this.cube.getColor();
	}
	
	
	/**
	 * Override to define what effect this Face will have on a Player's turn.
	 * 
	 * @param turn the turn
	 * @throws ForcedTurnEndException if something (say, too many shotguns) ends the turn
	 */
	public abstract void effect(PlayerTurn turn) throws ForcedTurnEndException;
}
