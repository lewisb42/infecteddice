package edu.westga.infecteddice.model;

import java.awt.Color;
import java.util.Random;

/**
 * A green Cube with 3 brains, 1 shotgun, and 2 runners
 * @author lewisb
 *
 */
public class GreenCube extends Cube {
    
    /**
     * Give us a random hashId.
     */
    private static final int hashId = (new Random()).nextInt();
    
	
	/**
	 * Creates a green cube with the appropriate faces.
	 */
	public GreenCube() {
		super();
		for (int i = 0; i < 3; i++) {
			super.addFace(new BrainFace(this));
		}
		super.addFace(new ShotgunFace(this));
		super.addFace(new FootstepsFace(this));
		super.addFace(new FootstepsFace(this));
	}
	
	// overridden to properly implement the hashCode-equals contrace
	@Override
	public int hashCode() {
	    return GreenCube.hashId;
	}

    @Override
    public Color getColor() {
        return Color.GREEN;
    }
}
