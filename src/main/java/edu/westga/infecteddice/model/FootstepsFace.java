package edu.westga.infecteddice.model;

import edu.westga.infecteddice.controllers.PlayerTurn;

/**
 * A Face representing footsteps of a runner who got away.
 * 
 * @author lewisb
 *
 */
public class FootstepsFace extends Face {

	/**
	 * Creates a FootstepsFace on the given cube.
	 * 
	 * @param cube the enclosing cube
	 */
	public FootstepsFace(Cube cube) {
		super(cube);
	}
	
	@Override
	public void effect(PlayerTurn turn) {
		turn.escaped(this);	
	}

}
